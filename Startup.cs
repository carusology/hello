﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace Hello {

    public class Startup {

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940

        public void ConfigureServices(IServiceCollection services) {}

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env) {
            app.UseDeveloperExceptionPage();

            app.Run(async (context) => {
                using (var client = new HttpClient()) {
                    var response = await client.GetAsync("http://www.google.com");
                    var statusCode = response.StatusCode;

                    Console.WriteLine($"Respond {statusCode:D} {statusCode:G}");

                    context.Response.StatusCode = (int)statusCode;
                    await context.Response.WriteAsync(await response.Content.ReadAsStringAsync());
                }
            });
        }

    }

}
